/*
 * Copyright (c) 2008 - 2013 10gen, Inc. <http://10gen.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package course;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.QueryBuilder;

public class BlogPostDAO {
    DBCollection postsCollection;

    public BlogPostDAO(final DB blogDatabase) {
        postsCollection = blogDatabase.getCollection("posts");
    }

    // Return a single post corresponding to a permalink
    public DBObject findByPermalink(String permalink) {

        DBObject post = null;
        QueryBuilder queryBuilder = QueryBuilder.start("permalink").is(permalink);

        post = postsCollection.findOne(queryBuilder.get());

        return post;
    }

    // Return a list of posts in descending order. Limit determines
    // how many posts are returned.
    public List<DBObject> findByDateDescending(int limit) {

        List<DBObject> posts = new ArrayList<DBObject>();
        
		// Cursor
		DBCursor cursor = postsCollection.find().sort(new BasicDBObject("date", -1)).limit(limit);
		
		try {
			while(cursor.hasNext()) {
				BasicDBObject doc = (BasicDBObject) cursor.next();
				posts.add(doc);
			}
		} finally {
			cursor.close();
		}
        // Return a list of DBObjects, each one a post from the posts collection

        return posts;
    }


    public String addPost(String title, String body, List<String> tags, String username) {

        System.out.println("inserting blog entry " + title + " " + body);

        String permalink = title.replaceAll("\\s", "_"); // whitespace becomes _
        permalink = permalink.replaceAll("\\W", ""); // get rid of non alphanumeric
        permalink = permalink.toLowerCase();


        BasicDBObject post = new BasicDBObject();
        // XXX HW 3.2, Work Here
        // Remember that a valid post has the following keys:
        // author, body, permalink, tags, comments, date
        //
        // A few hints:
        // - Don't forget to create an empty list of comments
        // - for the value of the date key, today's datetime is fine.
        // - tags are already in list form that implements suitable interface.
        // - we created the permalink for you above.

        // Build the post object and insert it

        post.append("title", title);
        post.append("author", username);
        post.append("body", body);
        post.append("permalink", permalink);
        post.append("date", new Date());
        ArrayList<BasicDBObject> comments = new ArrayList<BasicDBObject>();
        post.append("comments", comments);
        if (tags != null) {
        	post.append("tags", tags);
        } else {
        	post.append("tags", new ArrayList<String>());
        }
        postsCollection.insert(post);
        
//        {
//        	"_id" : ObjectId("513d396da0ee6e58987bae74"),
//        	"title" : "Martians to use MongoDB",
//        	"author" : "andrew",
//        	"body" : "Representatives from the planet Mars announced today that the planet would adopt MongoDB as a planetary standard. Head Martian Flipblip said that MongoDB was the perfect tool to store the diversity of life that exists on Mars.",
//        	"permalink" : "martians_to_use_mongodb",
//        	"tags" : [
//        		"martians",
//        		"seti",
//        		"nosql",
//        		"worlddomination"
//        	],
//        	"comments" : [ ],
//        	"date" : ISODate("2013-03-11T01:54:53.692Z")
//        }

        return permalink;
    }




   // White space to protect the innocent








    // Append a comment to a blog post
    public void addPostComment(final String name, final String email, final String body,
                               final String permalink) {

        // XXX HW 3.3, Work Here
        // Hints:
        // - email is optional and may come in NULL. Check for that.
        // - best solution uses an update command to the database and a suitable
        //   operator to append the comment on to any existing list of comments

    	// Se construye la nueva entrada del array
    	BasicDBObjectBuilder arrayEntryBuilder = BasicDBObjectBuilder.start("author", name).add("body", body);
    	if (email != null) {
    		arrayEntryBuilder.add("email", null);
    	}

    	// Se prepara la expresion de query
        QueryBuilder queryBuilder = QueryBuilder.start("permalink").is(permalink);
        
        // Operacion para incluir una nueva entrada en un array
        BasicDBObjectBuilder pushOperationBuilder = BasicDBObjectBuilder.start("$push", 
        		new BasicDBObject("comments", arrayEntryBuilder.get()));
        
        // Update
        postsCollection.update(queryBuilder.get(), pushOperationBuilder.get());

    }


}
