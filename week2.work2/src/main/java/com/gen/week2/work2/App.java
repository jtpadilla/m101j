package com.gen.week2.work2;

import java.net.UnknownHostException;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.QueryBuilder;
import com.mongodb.ServerAddress;

public class App {
	
	public static void main(String[] args) throws UnknownHostException {

		// Conexion con el servidor
		MongoClient client = new MongoClient(new ServerAddress("localhost", 27017));

		// Se obtiene la coleccion
		DB database = client.getDB("test");
		DBCollection collection = database.getCollection("grades");
		
		// Se instancia proporcinando la colleccion.
		App app = new App(collection);

		// Se procesan sus documentos
		app.scan();
		
	}
	
	private DBCollection collection;
	
	private App(DBCollection collection) {
		this.collection = collection;
	}

	private void scan() throws UnknownHostException {
		
		// Query
		QueryBuilder queryBuilder = QueryBuilder.start("type").is("homework");
		
		// Order
		BasicDBObject orderBy = new BasicDBObject().append("student_id", 1).append("score", 1);
		
		// Cursor
		BasicDBObject last = null;
		DBCursor cursor = collection.find(queryBuilder.get()).sort(orderBy);
		try {
			while(cursor.hasNext()) {
				// Se obtiene la siguiente tupla
				DBObject dbo = cursor.next();
				BasicDBObject doc = (BasicDBObject) dbo;
				// Tratando el resultado del cursor
				if (last == null) {
					// Si es la primera tupla del cursor, se anota
					last = doc;
				} else {
					// Si cambia de estudiante hay que borrar la ultima tupla.
					if (last.getInt("student_id") != doc.getInt("student_id")) {
						remove(last);
						last = doc;
					} else {
						System.out.println("Se mantiene..: " + doc.toString());
					}
				}
			}
			remove(last);
		} finally {
			cursor.close();
		}
		
	}
	
	private void remove(BasicDBObject doc) {
		System.out.println("Se borra.....: " + doc.toString());
		System.out.println();
		QueryBuilder queryBuilder = QueryBuilder.start("_id").is(doc.getObjectId("_id"));
		collection.remove(queryBuilder.get());
	}
	
}
