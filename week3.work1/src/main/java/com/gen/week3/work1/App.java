package com.gen.week3.work1;

import java.net.UnknownHostException;
import java.util.ArrayList;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.QueryBuilder;
import com.mongodb.ServerAddress;

public class App {
	
	public static void main(String[] args) throws UnknownHostException {

		// Conexion con el servidor
		MongoClient client = new MongoClient(new ServerAddress("localhost", 27017));

		// Se obtiene la coleccion
		DB database = client.getDB("school");
		DBCollection collection = database.getCollection("students");
		
		// Se instancia proporcinando la colleccion.
		App app = new App(collection);

		// Se procesan sus documentos
		app.updateDocs();
		
	}
	
	private DBCollection collection;
	
	private App(DBCollection collection) {
		this.collection = collection;
	}
	
	private void updateDocs() {
		
		QueryBuilder queryBuilder;

		// Cursor
		DBCursor cursor = collection.find();
		
		try {
			while(cursor.hasNext()) {
				
				// Se obtiene el siguiente documento
				BasicDBObject doc = (BasicDBObject) cursor.next();
				System.out.println("Antes: " + doc.toString());

				// Se modifica el documento
				BasicDBObject updatedDoc = removeLowestScoreSubdocument(doc);
				System.out.println("Despues: " + updatedDoc);
				System.out.println();
				
				// Se persiste la modificacion
				queryBuilder = QueryBuilder.start("_id").is(updatedDoc.get("_id"));
				collection.update(queryBuilder.get(), updatedDoc);
				
			}
		} finally {
			cursor.close();
		}
	}
	
	private BasicDBObject removeLowestScoreSubdocument(BasicDBObject doc) {
		int lowerIndex = 0;
		BasicDBObject lowerObject = null;
		
		ArrayList<BasicDBObject> scores = (ArrayList<BasicDBObject>) doc.get("scores");

		// Se busca la nota mas baja.
//		System.out.println("antes: " + scores.toString());
		for (int i = 0; i < scores.size(); i++) {
			if (scores.get(i).getString("type").equals("homework")) {
				if (lowerObject == null) {
					lowerObject = scores.get(i);
					lowerIndex = i;
				} else {
					if (scores.get(i).getDouble("score") < lowerObject.getDouble("score")) {
						lowerObject = scores.get(i);
						lowerIndex = i;
					}
				}
			}
		}
		
		// Se elimina (si la hay)
		if (lowerObject != null) {
			scores.remove(lowerIndex);
		}
//		System.out.println("Despues: " + scores.toString());
		
		// Se retorna el objeto modificado.
		doc.removeField("scores");
		doc.append("scores", scores);
		
		return doc;
		
	}
	
}
