package week7.work7;

import java.net.UnknownHostException;
import java.util.Iterator;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.QueryBuilder;
import com.mongodb.ServerAddress;

public class App {
	
	public static void main(String[] args) throws UnknownHostException {
		
		App app = new App();
		
		MongoClient client = new MongoClient(new ServerAddress("localhost", 27017));

		app.database = client.getDB("test");
		app.images = app.database.getCollection("images");
		app.albuns = app.database.getCollection("albuns");
		
		app.processAllImages();
		
		client.close();
	}

	private DB database;
	private DBCollection images;
	private DBCollection albuns;
	
	private int imageCount = 0;
	private int orphanCount = 0;
	
	private App() {
	}
	
	private void processAllImages() {
		// Creamos un cursor para obtener todas las imagenes (sin filtro)
		DBCursor imagesCursor = images.find();
		// Obtenemos cada una de las imaganes
		for (Iterator<DBObject> iteratorImages = imagesCursor.iterator(); iteratorImages.hasNext(); ) {
			DBObject imageDBObject = iteratorImages.next();
			// Si la imagen es huerfana
			if (isOrphanImage(imageDBObject)) {
				// La borramos
				removeImage(imageDBObject);
			}
			// Se contabiliza la imagen procesada
			imageCount++;
		}
		// Resumen
		System.out.println(String.format("%d images processed, %d imagenes huerfanas", imageCount, orphanCount));
	}
	
	private boolean isOrphanImage(DBObject imageDBObject) {
		DBCursor albunsCursor = null;
		try {
			// Creamos un cursor para obtener los albuns que contienen esta imagen
			DBObject albunsQuery = QueryBuilder.start("images").is(imageDBObject.get("_id")).get();
			albunsCursor = albuns.find(albunsQuery);
			// Hay algun albun para esta imagen 
			return !albunsCursor.hasNext();
		} finally {
			if (albunsCursor != null) {
				albunsCursor.close();
				albunsCursor = null;
			}
		}
	}
	
	private void removeImage(DBObject imageDBObject) {
		images.remove(QueryBuilder.start("_id").is(imageDBObject.get("_id")).get());
		orphanCount++;
	}
	
}
