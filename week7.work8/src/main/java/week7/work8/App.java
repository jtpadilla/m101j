package week7.work8;

import java.net.UnknownHostException;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

public class App {
	
	public static void main(String[] args) throws UnknownHostException {

		// Prepara todo lo necesario para poder trabajar con la coleccion
		MongoClient c = new MongoClient(new MongoClientURI("mongodb://localhost"));
		DB db = c.getDB("test");
		DBCollection animals = db.getCollection("animals");

		// 1) Inserta el documento y generara el _id automaticamente.
		BasicDBObject animal = new BasicDBObject("animal", "monkey");
		animals.insert(animal);
		animal.removeField("animal");
		
		// 2) Este comando generara un error porque el _id es el que se acaba de insertar en 1).
		animal.append("animal", "cat");
		animals.insert(animal);
		
		// 3) Este comando generara un error porque el _id ya se inserto en 1)
		animal.removeField("animal");
		animal.append("animal", "lion");
		animals.insert(animal);
		
		System.out.println("He aprobado, cabrones");

	}
	
}
